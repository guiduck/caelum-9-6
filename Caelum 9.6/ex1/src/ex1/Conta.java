package ex1;

public abstract class Conta {
	
	protected double saldo;
	
	void deposita(double valor) {
		this.saldo += valor;
	}

	public double getSaldo() {
		// TODO Auto-generated method stub
		return this.saldo;
	}
	
	public abstract void atualiza(double taxaSelic);
	
}
